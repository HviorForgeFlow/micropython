# Micropython ESP8266 NODEMCU

## References

* <https://blog.miguelgrinberg.com/post/micropython-and-the-internet-of-things-part-i-welcome>

* <https://github.com/peterhinch/micropython-async>

## Quickstart

1. Create virtual environment and install dependencies:

    ```bash
    virtualenv -p python3 .env
    source .env/bin/activate
    pip install -r requeriments.txt
    ```

2. Format NODEMCU with bin file (using /dev/ttyUSBX correctly):

    2.1. Erase Flash:

    ```bash
    esptool.py --port /dev/ttyUSB0 erase_flash
    ```

    2.2. Flash micropython-bin v1.11

    ```bash
    esptool.py --port /dev/ttyUSB0 write_flash 0 bin/esp8266-20190529-v1.11.bin 
    ```

3. Open REPL session:

    ```bash
    rshell --port /dev/ttyUSB0
    ```

    Inside rshell we can type `help` to get information about available commands.

    Common commands usage is:

    * Access data inside NODEMCU

        ```bash
        ls /pyboard
        ```

    * Copy files from computer to NODEMCU

        ```bash
        cp examples/server_example.py /pyboard
        ```

    * Remove files and so do on:

4. Run Python console using `repl`:

    ```shell
    repl

    Entering REPL. Use Control-X to exit.
    >
    MicroPython v1.11-8-g48dcbbe60 on 2019-05-29; ESP module with ESP8266
    Type "help()" for more information.
    >>>
    ```

5. Have fun!

## USING NODEMCU BOARD PIN

The first example we can try once with have `repl` running is:

```python
from machine import Pin
led = machine.Pin(2, machine.Pin.OUT)
```

Flash button read status
```python
from machine import Pin
flash = machine.Pin(0, Pin.IN)
flash.value() # 0 when pulse.
```

The Led will be automatically turn on, default state is 0. To turn it off:

```python
led.value(1)
led.on()
```

And turn it on again:

```python
led.value(0)
led.off()
```

## USING OLED DISPLAY SH1106

According with [SH1106 by robert-hh](https://github.com/robert-hh/SH1106) library (I've just tried using I2C connection).

* SDA to D2
* SCK to D1
* GND to G
* VDD to 3V

Copy SH1106 library:

```bash
cp custom-libs/SH1106/sh1106.py /pyboard
```

Then, we init OLED displays using:

```python
# Init SH1106
i2c = I2C(scl=Pin(5), sda=Pin(4), freq=400000)
display = sh1106.SH1106_I2C(128, 64, i2c, Pin(16), 0x3c)
display.sleep(False)
```

For [more](examples/sh1106/read_mfrc522_async.py) information.

## USING RFID READER MFRC522

According with [MFRC522 by wendlers](https://github.com/wendlers/micropython-mfrc522) library.

Copy MFRC522 library:

```bash
cp custom-libs/micropython-mfrc522/mfrc522.py /pyboard
```

[TODO]

## NODEMCU PINOUT

To refer to pins using Micropython on a NODEMCU we must use GPIO reference, but just the number.

* GPIO4 -> 4

![NODEMCU PINOUT](esp8266-pinout.jpg)

** Notice that GPI02 -> 2 is connected with on-board LED
