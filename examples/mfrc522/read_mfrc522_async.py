import mfrc522
import uasyncio as asyncio

async def do_read_esp8266():
	rdr = mfrc522.MFRC522(14, 13, 12, 0, 2)
	while True:
		(stat, tag_type) = rdr.request(rdr.REQIDL)
		if stat == rdr.OK:

			(stat, raw_uid) = rdr.anticoll()

			if stat == rdr.OK:
				print("New card detected")
				print("  - tag type: 0x%02x" % tag_type)
				print("  - uid	 : 0x%02x%02x%02x%02x" % (raw_uid[0], raw_uid[1], raw_uid[2], raw_uid[3]))
				print("")

				if rdr.select_tag(raw_uid) == rdr.OK:

					key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]

					if rdr.auth(rdr.AUTHENT1A, 8, key, raw_uid) == rdr.OK:
						print("Address 8 data: %s" % rdr.read(8))
						rdr.stop_crypto1()
					else:
						print("Authentication error")
				else:
					print("Failed to select tag")
		await asyncio.sleep(1)

loop = asyncio.get_event_loop()
loop.create_task(do_read_esp8266())
loop.run_forever()