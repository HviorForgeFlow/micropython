import network
import socket
from mputils import _get_unique_id_hex


def oot_config():

    uid = _get_unique_id_hex()
    ap = network.WLAN(network.AP_IF)  # create access-point interface
    ap.active(True)
    # set the ESSID of the access point
    ap.config(essid='OoT_AP_%s' % uid, password='forgeflow')
    while ap.active() == False:
        pass

    print('Connection successful')
    print(ap.ifconfig())

    def web_page():
        html = """
        <!DOCTYPE html>
        <html>
        <body>

        <h2>ESP8266 Nodemcu Micropython Web Handler</h2>

        <form action="/" method="POST">
        <fieldset>
            <legend>OoT Configuration:</legend>
            Odoo url template connection:<br>
            <input type="text" name="odoo_url" value="www.eficent.com" placeholder="Odoo URL" required>
            <br>
            Device Configuration:<br>
            <input type="text" name="devcfg" value="RAS" required>
            <br>
        </fieldset>
        <fieldset>
            <legend>Wireless Configuration:</legend>
            ESSID name: <br>
            <input type="text" name="essid" value="AndroidAP4508" placeholder="ESSID" required>
            <br>
            Password:<br>
            <input type="text" name="password" value="jornadas" placeholder="Password" required>
            <br>
        </fieldset>
        <input type="submit" value="Submit">
        </form>

        </body>
        </html>
        """
        return html

    while True:
        cl, addr = s.accept()
        print('client connected from', addr)
        data = cl.recv(1024)
        data = data.decode('utf-8')
        print(data)
        print(data.startswith('POST'))
        if data.startswith('POST'):
            pos = data.rfind('odoo_url')
            if pos:
                data = data[pos:]
                data = data.split('&')
                result = {}
                for rec in data:
                    print(rec)
                    params = rec.split('=')
                    result[params[0]] = params[1] if len(params) == 2 else ''
                print(result) # {'devcfg': 'RAS', 'essid': 'AndroidAP4508', 'odoo_url': 'www.eficent.com', 'password': 'jornadas'}
                success_html = """
                <!DOCTYPE html>
                    <html>
                    <body>

                    <h2> Configuring....! </h2>

                    </body>
                    </html
                """
                response = success_html
                cl.send(response)
                cl.close()
                ap.active(False)
                return result
        # elif data.startswith('GET'):
        response = web_page()
        cl.send(response)
        cl.close()


def main():
    return oot_config()


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

print('Starting socket')
connection = False
while not connection:
    res = main()
    sta_if = network.WLAN(network.STA_IF)
    sta_if.active(True)
    while sta_if.active() == False:
        pass
    sta_if.connect(res['essid'], res['password'])
    if sta_if.isconnected():
        print('I\'m conntected')
        connection = True
    else:
        res = main()
print('Finishing...')
