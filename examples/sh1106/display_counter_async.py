from machine import Pin, I2C
import sh1106
import uasyncio as asyncio

async def do_display_counter():
    count = 0
    while True:
        count += 1
        display.fill(0)
        display.text('count %s' % count , 0, 0, 1)
        display.show()
        await asyncio.sleep(1)

# Init SH1106
i2c = I2C(scl=Pin(5), sda=Pin(4), freq=400000)
display = sh1106.SH1106_I2C(128, 64, i2c, Pin(16), 0x3c)
display.sleep(False)
# Run counter
loop = asyncio.get_event_loop()
loop.create_task(do_display_counter())
loop.run_forever()