import network
import socket
from mputils import _get_unique_id_hex


def oot_config():

    uid = _get_unique_id_hex()
    ap = network.WLAN(network.AP_IF)  # create access-point interface
    ap.active(True)
    # set the ESSID of the access point
    ap.config(essid='OoT_AP_%s' % uid, password='forgeflow')
    while ap.active() == False:
        pass

    print('Connection successful')
    print(ap.ifconfig())

    def web_page():
        html = """
        <!DOCTYPE html>
        <html>
        <body>

        <h2>ESP8266 Nodemcu Micropython Web Handler</h2>

        <form action="/" method="POST">
        <fieldset>
            <legend>OoT Configuration:</legend>
            Odoo url template connection:<br>
            <input type="text" name="odoo_url" value="www.eficent.com" placeholder="Odoo URL" required>
            <br>
            Device Configuration:<br>
            <input type="text" name="devcfg" value="RAS" required>
            <br>
        </fieldset>
        <fieldset>
            <legend>Wireless Configuration:</legend>
            ESSID name: <br>
            <input type="text" name="essid" value="AndroidAP4508" placeholder="ESSID" required>
            <br>
            Password:<br>
            <input type="text" name="password" value="jornadas" placeholder="Password" required>
            <br>
        </fieldset>
        <input type="submit" value="Submit">
        </form>

        </body>
        </html>
        """
        return html

    w_essid = ''
    w_pass = ''
    o_url = ''
    o_dev = ''

    while True:
        cl, addr = s.accept()
        print('client connected from', addr)
        data = cl.recv(1024)
        data = data.decode('utf-8')
        print(data)
        print(data.startswith('POST'))
        if data.startswith('POST'):
            for i in range(3):
                j = data.rfind("&")
                k = data.rfind("=")
                print(data[j+1:k])
                print(data[k+1:])
                if data[j+1:k] == 'password':
                    w_pass = data[k+1:]
                elif data[j+1:k] == 'essid':
                    w_essid = data[k+1:]
                elif data[j+1:k] == 'devcfg':
                    o_dev = data[k+1:]
                data = data[:j]
            k = data.rfind("=")
            o_url = data[k+1:]
            print(o_url)
            if w_essid and w_pass and o_dev and o_url:
                success_html = """
                <!DOCTYPE html>
                    <html>
                    <body>

                    <h2> Configuring....! </h2>

                    </body>
                    </html
                """
                response = success_html
                cl.send(response)
                cl.close()
                return w_essid, w_pass, o_url, o_dev
        # elif data.startswith('GET'):
        response = web_page()
        cl.send(response)
        cl.close()


def main():
    return oot_config()


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

print('Starting socket')
w_essid, w_pass, o_url, o_dev = main()
sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)
while sta_if.active() == False:
    pass
sta_if.connect(w_essid, w_pass)
if sta_if.isconnected():
    print('I\'m conntected')
else:
    w_essid, w_pass, o_url, o_dev = main()
