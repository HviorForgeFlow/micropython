import network

ap = network.WLAN(network.AP_IF) # create access-point interface
ap.config(essid='OoT_AP', password='qwertyuiop') # set the ESSID of the access point
ap.active(True)         # activate the interface


from http_ssl_server import main
main(use_stream=False)
