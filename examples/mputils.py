import machine
import binascii


def _get_unique_id_hex():

    def _get_unique_id_byte():
        return machine.unique_id()

    def byte_to_hex(unique_id):
        return binascii.hexlify(unique_id)

    def decode_byte_to_str(bytes):
        return bytes.decode('utf-8')

    return decode_byte_to_str(byte_to_hex(_get_unique_id_byte()))


